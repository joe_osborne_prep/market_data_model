import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by alumniCurie02 on 22/09/2017.
 */
public class VirtualStockExchange {

    private List <TradeData> instrumentList;

    private static VirtualStockExchange vse = null;

    private VirtualStockExchange(){instrumentList = new ArrayList<>();}

    //get an instance of the current class or create one if it has not been created
    public static VirtualStockExchange getInstance() {
        if(vse == null){
            return new VirtualStockExchange();
        }else{
            return vse;}
    }

    //generate random trades for the instruments supplied
    private void generatePoints(Integer DataPoints){
        for(int i=0; i < DataPoints;i++){
            Random r = new Random();
            equityTrade equityTrade = getNewTrade(i);
            int randoNum = r.nextInt((instrumentList.size()-1)+1);
            instrumentList.get(randoNum).addTrade(equityTrade);
        }
    }


    //supply random data for each new trade
    private equityTrade getNewTrade (int dataNum){
        Random r = new Random();
        Date today = new Date();
        DateFormat dateFormat = new SimpleDateFormat("EEEEE dd MMMMM yyyy"); // format pattern to display the date
        String date = dateFormat.format(today);// supply today's date
        Time time = new Time(28800000 + (21000 * dataNum));//09:00:00 + 21secs for each new trade - smallerNumber=smaller time gaps
        double randVal = 50 + (100)*r.nextDouble();
        double price = randVal * 100;
        boolean cross = r.nextBoolean();
        int vol = 100 + r.nextInt(1000-100+1);
        equityTrade equityTrade = new equityTrade(date,time,price,cross,vol);

        return equityTrade;
    }


    //run the simulation of the day
    public void simDay (){
        TradeData vodafone = new TradeData("VODAFONE GROUP PLC","VOD");
        TradeData bt = new TradeData("BT Group PLC","BT/A");
        TradeData hsbc = new TradeData("HSBC Holdings PLC","HSBA.L");

        //add instrument to list
        instrumentList.add(vodafone);
        instrumentList.add(bt);
        instrumentList.add(hsbc);

        //generate total number of data points/trades on the day
        generatePoints(500);
        for (int i=0; i<instrumentList.size();i++){
            instrumentList.get(i).printEquityData();
        }
    }

    //start the simulation and run
    public static void main(String[] args) {
        VirtualStockExchange virtualDay = VirtualStockExchange.getInstance();
        virtualDay.simDay();
    }

}