import java.sql.Time;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alumniCurie02 on 22/09/2017.
 */

abstract class instruments{

    private String name;
    private String ticker;

    public String getName(){return name;}
    public void setName(String name){this.name=name;}

    public String getTicker(){return ticker; }
    public void setTicker(String ticker) {this.ticker=ticker;}

}

//access the instruments data
abstract class trade{

    private String date;
    private Time time;
    private double price;
    private boolean isCrossTrade = false;
    private Integer totAmountTraded;
    public abstract void printTrades();

    public String getDate (){return date;}
    public void setDate (String date){this.date = date;}

    public Date getTime (){return time;}
    public void setTime (Time time){this.time = time;}

    public double getPrice (){return price;}
    public void setPrice (double price){this.price = price;}

    public boolean getIsCrossTrade(){return isCrossTrade;}
    public void setIsCrossTrade (boolean isCrossTrade){this.isCrossTrade=isCrossTrade;}

    public Integer getTotAmountTraded(){return totAmountTraded;}
    public void setTotAmountTraded(Integer totAmountTraded){this.totAmountTraded=totAmountTraded;}

}


class equityTrade extends trade{
    DecimalFormat df = new DecimalFormat("###.#####");

    //print trade data in the format shown
    @Override
    public void printTrades() {
        System.out.println("Date: " + getDate() + " Time: " + getTime() + " Price: " + df.format(getPrice()) +
                " Cross: " + getIsCrossTrade() + " Volume(units traded): " + getTotAmountTraded());
    }



// generate the trade based on data provided in VirtualStockExchange
    equityTrade(String date,Time time, double price,boolean cross,int traded){
        setDate(date);
        setTime(time);
        setPrice(price);
        setTotAmountTraded(traded);
        setIsCrossTrade(cross);
    }
}


public class TradeData extends instruments {

    private List<equityTrade> trades = new ArrayList<>();

    //add the instruments to a list to be used during the simulation
    public TradeData (String name, String ticker){
        setName(name);
        setTicker(ticker);
        trades = new ArrayList<>();
    }

    //print summarised data of the day for each instrument in the format shown below
    public void printEquityData(){
        DecimalFormat df = new DecimalFormat("###.#####");
        System.out.println("Equity name: " + getName() + " [Ticker]: " + getTicker());
        System.out.println("Open price: " + df.format(getOpenPrice()));
        System.out.println("Close price: " + df.format(getClosePrice()));
        System.out.println("Average price(exclude open and close): " + df.format(getAveragePrice()));
        System.out.println("Volume weighted average price: " + df.format(getVolumeWeightedAvgPrice()));
        System.out.println("Volume weighted average price (without internal crosses): " + df.format(getVolumeWeightedAvgPriceWithoutInternalCrosses()));

        System.out.println("\nEquity trades: ");
        for (equityTrade t : trades) {
            t.printTrades();
        }
        System.out.println("---------------------------------------------------------------------------------------------------------------------\n");
    }

    public void addTrade (equityTrade trade){
        trades.add(trade);
    }

    public double getOpenPrice (){
        if(trades.size()>1){
            return trades.get(0).getPrice();
        }
        else{
            return 0;
        }
        }

    public double getClosePrice (){
        if(trades.size()>1){
            return trades.get(trades.size()-1).getPrice();        }
        else{
            return 0;
        }

        }

    public double getAveragePrice () {
        double avgPrice = 0;
        for (int i = 1; i < trades.size()-1; i++) {
            avgPrice = avgPrice + trades.get(i).getPrice();
        }
        avgPrice=avgPrice/(trades.size()-2);
        return avgPrice;
    }

    public double getVolumeWeightedAvgPrice (){
        double sumVolume = 0;
        double quant;
        double numerator=0;
        double price;
        for (int i = 0; i < trades.size(); i++) {
            price = trades.get(i).getPrice();
            quant = trades.get(i).getTotAmountTraded();
            numerator = numerator + (price*quant);
            sumVolume = sumVolume+quant;
        }
        double vwap = numerator/sumVolume;
        if(Double.isNaN(vwap)){
            return 0;}
        else{
            return vwap;
        }
    }

    public double getVolumeWeightedAvgPriceWithoutInternalCrosses (){
        double sumVolume = 0;
        double quant;
        double numerator=0;
        double price;
        for (int i = 0; i < trades.size(); i++) {
            if(trades.get(i).getIsCrossTrade()==false){
                price = trades.get(i).getPrice();
                quant = trades.get(i).getTotAmountTraded();
                numerator = numerator + (price*quant);
                sumVolume = sumVolume+quant;
            }
        }
        double vwap = numerator/sumVolume;
        if(Double.isNaN(vwap)){
            return 0;}
        else{
            return vwap;
        }

        }

}
